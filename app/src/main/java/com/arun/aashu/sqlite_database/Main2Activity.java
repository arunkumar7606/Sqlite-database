package com.arun.aashu.sqlite_database;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class Main2Activity extends AppCompatActivity {

    ListView listView;
    ArrayList arrayList=new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        listView=findViewById(R.id.listview1);

        Bundle b=getIntent().getExtras();
        arrayList=b.getStringArrayList("jadu");

        ArrayAdapter adapter=new ArrayAdapter(Main2Activity.this,android.R.layout.simple_list_item_1,arrayList);
        listView.setAdapter(adapter);



    }
}
