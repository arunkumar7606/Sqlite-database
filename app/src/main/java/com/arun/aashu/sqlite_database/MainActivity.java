package com.arun.aashu.sqlite_database;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    EditText edId, edNAme, edAddress;
    DataBaseFile db;
    Button btnAdd, btnGet, btnRecord;
    List<CriminalRecord> list;
    ArrayList<String> arrayList = new ArrayList<>();
//    ArrayAdapter<String> arrayAdapter;
//    AutoCompleteTextView at;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new DataBaseFile(this);

        edId = findViewById(R.id.editId);
        edNAme = findViewById(R.id.editName);
        edAddress = findViewById(R.id.editAddress);

        btnAdd = findViewById(R.id.addBtn);
        btnGet = findViewById(R.id.getBtn);
        btnRecord = findViewById(R.id.btnRecord);
//        at=findViewById(R.id.autoText);

//        arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.select_dialog_item, arrayList);
//        at.setAdapter(arrayAdapter);
//        at.setThreshold(0);


        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int id = Integer.parseInt(edId.getText().toString());
                String name = edNAme.getText().toString();
                String disp = edAddress.getText().toString();

                CriminalRecord record = new CriminalRecord();

                record.setId(id);
                record.setName(name);
                record.setDisp(disp);

                db.addCriminalRecord(record);
                Toast.makeText(MainActivity.this, "Data Saved..", Toast.LENGTH_SHORT).show();

            }
        });

        btnGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                list = db.getAllCriminalList();


                for (CriminalRecord rec : list) {

                    arrayList.add("Id:-" + rec.getId()
                            + "\nName:-" + rec.getName() + "\ndiscription:-" + rec.getDisp());

                    Toast.makeText(MainActivity.this, "Id:-" + rec.getId()
                            + "\nName:-" + rec.getName() + "\ndiscription:-" + rec.getDisp(), Toast.LENGTH_SHORT).show();

//                   arrayList.add("\nName:-"+rec.getName());
//                   arrayAdapter.notifyDataSetChanged();
                }


                startActivity(new Intent(MainActivity.this, Main2Activity.class)
                        .putStringArrayListExtra("jadu", arrayList));


            }
        });

        btnRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String data = edId.getText().toString();

                try {

                    if (data.equals("")) {
                        edId.setError("Please Enter id");
                    } else {
                        int id = Integer.parseInt(data);
                        CriminalRecord rec = db.getSingleRecord(id);

                        Toast.makeText(MainActivity.this, "Id:-" + rec.getId()
                                + "\nName:-" + rec.getName() + "\ndiscription:-" + rec.getDisp(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(MainActivity.this, "No Record Found", Toast.LENGTH_SHORT).show();
                }
            }
        });

        findViewById(R.id.btnUpdate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int id=Integer.parseInt(edId.getText().toString());

                String desc=edAddress.getText().toString();
                String name=edNAme.getText().toString();
                CriminalRecord record=new CriminalRecord();

                record.setId(id);
                record.setDisp(desc);
                record.setName(name);

                db.UpdateRecord(record);
                Toast.makeText(MainActivity.this, "Record Updated", Toast.LENGTH_SHORT).show();

                edId.setText("");
                edAddress.setText("");

            }
        });

        findViewById(R.id.btnDelete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                int id=Integer.parseInt(edId.getText().toString());

                String desc=edAddress.getText().toString();
                String name=edNAme.getText().toString();
                CriminalRecord record=new CriminalRecord();

                record.setId(id);
                record.setDisp(desc);
                record.setName(name);

                db.deleteRecord();
                Toast.makeText(MainActivity.this, "Record deleted", Toast.LENGTH_SHORT).show();

                edId.setText("");
                edAddress.setText("");

            }
        });


    }

    @Override
    public void onBackPressed() {


        super.onBackPressed();
    }


}
