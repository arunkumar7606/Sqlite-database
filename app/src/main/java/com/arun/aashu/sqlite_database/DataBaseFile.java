package com.arun.aashu.sqlite_database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aAsHu on 4/10/2018.
 */

public class DataBaseFile extends SQLiteOpenHelper {

    //Variable name should be in capital letters..because it is standard
    //Because in java every constant and final variable is in capital

    CriminalRecord record;
    Context context;

    public static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "mydb";

    // database_name and Table_name must not have spaces...
    public static final String TABLE_NAME = "criminal_ka_data";

    // Columns_name...
    public static final String CRIMINAL_ID = "id";

    public static final String CRIMINAL_NAME = "name";

    public static final String CRIMINAL_DISCRIPTIONS = "disp";

    //When constructor run then dataBase created..

    public DataBaseFile(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        // CREATE TABLE JADU (ID NUMBER PRIMARY KEY,NAME TEXT,DISP TEXT);

        String query = "CREATE TABLE " + TABLE_NAME + "(" + CRIMINAL_ID + " NUMBER PRIMARY KEY," + CRIMINAL_NAME + " TEXT," + CRIMINAL_DISCRIPTIONS + " TEXT" + ");";

        sqLiteDatabase.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

        // DROP ye hamhare table ko haata ta hai , table k bhad onCreate fer se chla do
        // Onupgrade ,onCreate k just bhad chlta hai

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(sqLiteDatabase);

    }

    public void addCriminalRecord(CriminalRecord record) {

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(CRIMINAL_ID, record.getId());
        values.put(CRIMINAL_NAME, record.getName());
        values.put(CRIMINAL_DISCRIPTIONS, record.getDisp());

        sqLiteDatabase.insert(TABLE_NAME, null, values);

        sqLiteDatabase.close();

    }

    public List<CriminalRecord> getAllCriminalList() {

        SQLiteDatabase db = getReadableDatabase();

        //Blank Array
        ArrayList<CriminalRecord> records = new ArrayList<CriminalRecord>();
        String query = "SELECT * FROM " + TABLE_NAME;

        //Cursor goes from row by row
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {

            do {
                CriminalRecord rec = new CriminalRecord();
                rec.setId(cursor.getInt(0));
                rec.setName(cursor.getString(1));
                rec.setDisp(cursor.getString(2));

                // add records to list
                records.add(rec);


            } while (cursor.moveToNext());
        }

        return records;
    }

    public CriminalRecord getSingleRecord(int id) {

        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(TABLE_NAME,
                new String[]{CRIMINAL_ID, CRIMINAL_NAME, CRIMINAL_DISCRIPTIONS},
                CRIMINAL_ID + "=?",
                new String[]{String.valueOf(id)},
                null,
                null,
                null);

        if (cursor != null)
            cursor.moveToNext();

        record = new CriminalRecord(cursor.getInt(0),
                cursor.getString(1),
                cursor.getString(2));


        return record;
    }

    public void UpdateRecord(CriminalRecord record) {

        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(CRIMINAL_NAME,record.getName());
        values.put(CRIMINAL_DISCRIPTIONS,record.getDisp());

        db.update(TABLE_NAME,
                values,
                CRIMINAL_ID+ "=?",
                new String[]{String.valueOf(record.getId())});

        db.close();
    }

    public void deleteRecord(int id){

        SQLiteDatabase db=getWritableDatabase();
        db.delete(TABLE_NAME,CRIMINAL_ID + "=?",
                new String[]{String.valueOf(id)});
        db.close();

//        db.execSQL();


    }
}
