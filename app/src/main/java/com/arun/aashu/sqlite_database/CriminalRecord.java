package com.arun.aashu.sqlite_database;

/**
 * Created by aAsHu on 4/12/2018.
 */

public class CriminalRecord {

    int id;
    String name,disp;


    public CriminalRecord(int id, String name, String disp) {
        this.id = id;
        this.name = name;
        this.disp = disp;
    }

    public CriminalRecord() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisp() {
        return disp;
    }

    public void setDisp(String disp) {
        this.disp = disp;
    }
}
